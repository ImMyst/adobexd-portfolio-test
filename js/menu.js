(function () {
    let menu = document.querySelector('ul')
    let menuLink = document.querySelector('img')

    menuLink.addEventListener('click', function (e) {
        menu.classList.toggle('active')
        e.preventDefault();
    })
})()